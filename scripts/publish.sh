#!/bin/bash

echo publish;

# latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)
latestTag="11.231.321";

echo $latestTag;

MARJOR=$(echo $latestTag | grep -E -o "^[[:digit:]]+");
MINOR=$(echo $(echo $latestTag | grep -E -o "\.[[:digit:]]+\.") | grep -E -o "[[:digit:]]+");
PATCH=$(echo $latestTag | grep -E -o "[[:digit:]]+$");

echo MARJOR $MARJOR
echo MINOR $MINOR
echo PATCH $PATCH
