# Security Policy

## Supported Versions

| Version | Supported          |
| ------- | ------------------ |
| 1.x.x   | :white_check_mark: |
| 0.x.x   | :x: |

## Reporting a Vulnerability
Follow the link `https://github.com/Aleksandr-JS-Developer/purify-html/security/advisories/new` and fill out the form. If you are not sure how to fill out the form - just write what you can. Please allow time for correction (or correct it yourself) before posting any information publicly.
